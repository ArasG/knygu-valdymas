package lt.sda.academy;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.data.BookService;
import lt.sda.academy.data.ReviewService;
import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;
import lt.sda.academy.model.Review;
import lt.sda.academy.presentation.ApplicationGreeting;
import lt.sda.academy.presentation.AuthorChoice;
import lt.sda.academy.presentation.BookChoice;
import lt.sda.academy.presentation.ReviewChoice;
import lt.sda.academy.util.*;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainApplication {
    public static void main(String[] args) throws IOException {

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        BookService bookService = new BookService(sessionFactory);
        AuthorService authorService = new AuthorService(sessionFactory);
        ReviewService reviewService = new ReviewService(sessionFactory);
        PaginationUtil paginationUtil = new PaginationUtil();
        ImportCsvUtil importCsvUtil = new ImportCsvUtil();


        int choice = 0;
        while (choice != 4) {

            choice = ApplicationGreeting.selectMainMeniuWindow();
            switch (choice) {
                case 1: //AUTORIAI
                    int choiceFromAuthorMenu = AuthorChoice.selectAuthorsChoices();
                    switch (choiceFromAuthorMenu) {
                        case 1: //matyti visus autorius po 10
                            paginationUtil.printAuthorsWithPagination(authorService);
                            break;
                        case 2: //ieskoti autoriau pagal varda ar pavarde
                            authorService.searchAuthorByNameOrSurname();
                            break;
                        case 3: // issaugoti nauja autoriu
                            authorService.saveAuthor();
                            break;
                        case 4: //atnaujinti autoriu
                            int authorsToUpdateId = AuthorChoice.enterAuthorToUpdateId();
                            Author authorToUpdate = authorService.getAuthorById(authorsToUpdateId);
                            if (authorToUpdate == null) {
                                System.out.println("Autorius su tokiu ID neegzistuoja");
                            } else {
                                String authorsToUpdateName = AuthorChoice.enterFirstNameOfAuthorToUpdate();
                                String authorsToUpdateSurname = AuthorChoice.enterLastNameOfAuthorToUpdate();
                                authorService.updateAuthor(authorsToUpdateId, authorsToUpdateName, authorsToUpdateSurname);
                                AuthorChoice.authorUpdatedSuccessfully(authorToUpdate, authorsToUpdateName, authorsToUpdateSurname);
                            }

                            break;
                        case 5: // istrinti autoriu, jo knygas ir knygu atsiliepimus
                            int authorsToDeleteId = AuthorChoice.enterAuthorToDeleteId();
                            Author authorToDelete = authorService.getAuthorById(authorsToDeleteId);
                            if (authorToDelete == null) {
                                System.out.println("Autorius su tokiu ID neegzistuoja");
                            } else {
                                List<Book> booksOfAuthor = bookService.getBooksByAuthor(authorToDelete);
                                authorService.deleteAuthor(authorToDelete);
                                String allLines = "";
                                String reviewsToDelete = "";
                                for (Book b : booksOfAuthor) {
                                    allLines += b.toString();
                                    reviewService.deleteRevewByBookId(b.getId());
                                    List<Review> reviewsToBeDeleted = reviewService.getAllReviewsForOneBook(b);
                                    for (Review r : reviewsToBeDeleted) {
                                        reviewsToDelete += r.getId() + " " + r.getScore() + " " + r.getComment();
                                    }
                                }
                                AuthorChoice.authorDeletedSuccessfully(authorToDelete);
                                System.out.println("----Istrintos autoriaus knygos: " + allLines);
                                System.out.println("----Istrinti knygu atsiliepinai: " + reviewsToDelete);
                            }

                    }
                    break;
                case 2: //KNYGOS
                    int choiceFromBookMenu = BookChoice.selectBookChoice();
                    switch (choiceFromBookMenu) {
                        case 1:// matyti visas knygas
                            bookService.printAllBooks();
                            break;
                        case 2:

                            break;
                        case 3: //knygos issaugojimas
                            bookService.saveBook();
                            break;
                        case 4: // knygos atnaujinimas
                            int updateBookChoice = BookChoice.updateBookChoice();
                            int bookForUpdateId = ScannerUtil.enterBooksIdForUpdate();
                            Book bookForUpdate = bookService.getBookById(bookForUpdateId);
                            while (bookForUpdate == null) {
                                System.out.println("\n----Knygos su tokiu ID nera. Bandykite dar karta: ");
                                bookForUpdateId = ScannerUtil.enterBooksIdForUpdate();
                                bookForUpdate = bookService.getBookById(bookForUpdateId);
                            }
                            switch (updateBookChoice) {
                                case 1:
                                    String newTitle = ScannerUtil.enterBooksNewTitle();
                                    bookService.updateBooksTitle(bookForUpdateId, newTitle);
                                    Book updatedBook = bookService.getBookById(bookForUpdateId);
                                    BookChoice.bookUpdatedSuccessfully(updatedBook);
                                    break;
                                case 2:
                                    String newDescription = ScannerUtil.enterBooksNewDescription();
                                    bookService.updateBooksDescription(bookForUpdateId, newDescription);
                                    updatedBook = bookService.getBookById(bookForUpdateId);
                                    BookChoice.bookUpdatedSuccessfully(updatedBook);
                                    break;
                                case 3:
                                    int newAuthorID = ScannerUtil.enterNewAuthorId();
                                    Author newAuthor = authorService.getAuthorById(newAuthorID);
                                    bookService.updateBooksAuthor(bookForUpdateId, newAuthor);
                                    updatedBook = bookService.getBookById(bookForUpdateId);
                                    BookChoice.bookUpdatedSuccessfully(updatedBook);
                                    break;
                            }
                            break;
                        case 5:// istrinti knyga ir jos reviews
                            int idForDelete = ScannerUtil.enterBooksIdForDelete();
                            Book bookForDelete = bookService.getBookById(idForDelete);
                            if (bookForDelete == null) {
                                System.out.println("Knygos su tokiu ID nera.");
                            } else {
                                bookService.deleteBook(bookForDelete);
                                reviewService.deleteRevewByBookId(bookForDelete.getId());
                                String reviewsToDelete = "";
                                List<Review> reviewsToBeDeleted = reviewService.getAllReviewsForOneBook(bookForDelete);
                                for (Review r : reviewsToBeDeleted) {
                                    reviewsToDelete += r.getId() + " " + r.getScore() + " " + r.getComment();
                                }
                                System.out.print("\n----Knyga sekmingai istrinta: ");
                                System.out.println(bookForDelete.toStringWithAuthorsName());
                                System.out.println("----Knygos atsiliepimai sekmingai istrinti: " + reviewsToDelete);
                            }
                            break;
                        case 6: //importuoti is failo
                            List<String> linesFromFile = importCsvUtil.readAllLines();
                            List<Author> savedAuthors = new ArrayList<>();
                            List<Book> savedBooks = new ArrayList<>();
                            for (String line : linesFromFile) {
                                Author author = importCsvUtil.extractAuthorFromFile(line);
                                boolean checkIfAuthorNotExists = authorService.checkIfAuthorNotExistById(author);
                                if (checkIfAuthorNotExists) {
                                    authorService.saveAuthor(author);
                                    savedAuthors.add(author);
                                }
                                Author authorFromDb = authorService.getAuthorByNameAndSurname(author.getFirstName(), author.getLastName());
                                Book book = importCsvUtil.extractBookFromFile(line, authorFromDb);
                                boolean checkIfBookNotExists = bookService.checkIfBookNotExists(book, authorFromDb.getId());
                                if (checkIfBookNotExists) {
                                    bookService.saveBook(book);
                                    savedBooks.add(book);
                                }
                            }
                            System.out.println("\nSUIMPORTUOTI AUTORIAI: ");
                            PrintUtil.printAuthors(savedAuthors);
                            System.out.println("\nSUIMPORTUOTOS KNYGOS: ");
                            PrintUtil.PrintBooks(savedBooks);


                    }
                    break;
                case 3: //REVIEW
                    int reviewChoice = ReviewChoice.selectReviewChoice();
                    switch (reviewChoice) {
                        case 1:// matyti visus ivertinimus
                            List<Review> reviews = reviewService.getAllReviews();
                            String allLines = "";
                            for (Review r : reviews) {
                                Book book = bookService.getBookById(r.getBookId());
                                String oneLine = reviewService.returnAllReviewsWithBookTitle(r, book);
                                allLines += oneLine;
                            }
                            System.out.println("\n----VISI IVERTINIMAI:----\n" + allLines);
                            break;
                        case 2: // matyti ivertinimus knygai pagal ID
                            int bookId = ScannerUtil.enterBookId();
                            Book bookToCheckReview = bookService.getBookById(bookId);
                            if (bookToCheckReview == null) {
                                System.out.println("Knygos su tokiu ID nera.");
                            } else {
                                List<Review> reviewsOfOneBook = reviewService.getAllReviewsForOneBook(bookToCheckReview);
                                String allLinesForReviews = "";
                                for (Review r : reviewsOfOneBook) {
                                    String oneLine = reviewService.returnAllReviewsWithBookTitle(r, bookToCheckReview);
                                    allLinesForReviews += oneLine;
                                }
                                System.out.println("\n----Knygos " + bookToCheckReview.getTitle() + " visi ivertinimai:----");
                                System.out.println(allLinesForReviews);
                            }
                            break;
                        case 3: // prideti nauja ivertinima
                            int bookIdForReview = ScannerUtil.enterBooksIdForReview();
                            Book bookForReview = bookService.getBookById(bookIdForReview);
                            if (bookForReview == null) {
                                System.out.println("\n----Knygos su tokiu ID nera.\n");
                            } else {
                                int booksScore = ScannerUtil.enterScoreForBook();
                                String comment = ScannerUtil.enterBookComment();
                                reviewService.saveReview(bookIdForReview, booksScore, comment);
                                System.out.println("----Ivertinimas sekmingai irasytas.");
                            }
                            break;

                    }
                case 4: //for test


            }

        }

    }
}


