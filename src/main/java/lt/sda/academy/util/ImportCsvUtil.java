package lt.sda.academy.util;

import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ImportCsvUtil {


    public List<String> readAllLines() throws IOException {
        return Files.readAllLines(Paths.get("C:\\Users\\Aras\\knygu-valdymas2\\src\\main\\resources\\readFile.csv"));
    }

    public Author extractAuthorFromFile(String lineFromFile) {
        String[] oneLineSplit = lineFromFile.split(",");
        String firstName = oneLineSplit[0];
        String lastname = oneLineSplit[1];
        return new Author(firstName, lastname);
    }


    public Book extractBookFromFile(String lineFromFile, Author author) {
        String[] oneLineSplit = lineFromFile.split(",");
        String bookTitle = oneLineSplit[2];
        String bookDescription = oneLineSplit[3];
        return new Book(bookTitle, bookDescription,author);
    }


}

