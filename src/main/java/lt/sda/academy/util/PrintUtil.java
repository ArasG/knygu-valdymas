package lt.sda.academy.util;

import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;

import java.util.List;

public class PrintUtil {

    public static void printAuthors(List<Author> authorList) {
        for (Author author : authorList) {
            System.out.println(author.toString());
        }
        System.out.println("VISO AUTORIU: " + authorList.size());
    }

    public static void PrintBooks(List<Book> bookList) {
        for (Book book : bookList) {
            System.out.println(book.toStringWithAuthorsName());
        }
        System.out.println("VISO KNYGU: " + bookList.size() + "\n");
    }
}

