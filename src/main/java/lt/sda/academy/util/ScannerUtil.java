package lt.sda.academy.util;

import lt.sda.academy.data.BookService;
import lt.sda.academy.model.Book;

import java.util.Scanner;

public class ScannerUtil {
    public static final Scanner SCANNER = new Scanner(System.in);


    public static int getAuthorIdInputFromUser() {
        System.out.print("Iveskite autoriaus id: ");
        return SCANNER.nextInt();
    }

    public static String getAuthorFirstNameInputFromUser() {
        System.out.println("Iverskite autoriaus varda: ");
        return SCANNER.next();
    }

    public static String getAuthorLastNameInputFromUser() {
        System.out.println("Iverskite autoriaus pavarde: ");
        return SCANNER.next();
    }

    public static int enterNewAuthorId() {
        System.out.println("----Iveskite naujo autoriaus ID ");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static String getStringInput() {
        return SCANNER.next();
    }

    public static String getSringInputForPaginationUtil() {
        System.out.println("Rodyti sekancius desimt irasu? Y - taip; N - exit");
        String result = "";
        while (!result.equals("y") && !result.equals("n")) {
            result = SCANNER.next();
            result = result.toLowerCase();
        }
        return result;
    }

    public static String getBookTitleInputFromUser() {
        System.out.print("Iveskite knygos pavadinima: ");
        return SCANNER.next();
    }

    public static String getBookDesciptionInputFromUser() {
        System.out.print("Iveskite knygos aprasyma: ");
        return SCANNER.next();
    }

    public static int enterBooksIdForUpdate() {
        System.out.println("----Iveskite norimos redeguoti knygos ID: ");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static int enterBooksIdForReview() {
        System.out.println("----Iveskite knygos ID, kuriai norite irasyti atsiliepima: ");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static String enterBooksNewTitle() {
        System.out.println("----Iveskite knygos nauja pavadinima: ");
        return ScannerUtil.SCANNER.next();
    }

    public static String enterBooksNewDescription() {
        System.out.println("----Iveskite nauja knygos aprasyma ");
        return ScannerUtil.SCANNER.next();
    }

    public static int enterBooksIdForDelete() {
        System.out.println("----Iverskite norimos istrinti knygos ID: ");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static String enterBookComment() {
        System.out.println("----Iveskite knygos komentara: ");
        SCANNER.nextLine();
        return SCANNER.nextLine();
    }

    public static int enterBookId() {
        System.out.println("----Iveskite knygos ID: ");
        return SCANNER.nextInt();
    }

    public static int enterScoreForBook() {
        int score = 11;
        while (score < 0 || score > 10) {
            System.out.println("----Iveskite knygai skiraimus balus (nuo 0 iki 10):  ");
            score = SCANNER.nextInt();
            if (score < 0 || score > 10) {
                System.out.println("----Irasas neteisingas. Galimi balai nuo 0 iki 10; Bandykite dar karta;");
            }
        }
        return score;
    }

}
