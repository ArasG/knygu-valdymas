package lt.sda.academy.util;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.model.Author;

public class PaginationUtil {

    public void printAuthorsWithPagination(AuthorService authorService) {
        int numberOfEntriesInPage = 10;
        int totalEntries = (int) authorService.getTotalCountOfAuthors();
        int totalPages = (int) Math.ceil((double) totalEntries / numberOfEntriesInPage);
        int lastEntriesOnLastPage = totalEntries % 10;
        int page = 1;
        int firstResult = 0;
        String action = "y";
        while ((page <= totalPages && action.equals("y"))) {
            authorService.printAuthorsPagination(firstResult);
            firstResult += 10;
            if (page == totalPages) {
                firstResult += lastEntriesOnLastPage;
            }
            page += 1;
            if (page <= totalPages) {
                action = ScannerUtil.getSringInputForPaginationUtil();
            }
        }
        if (!action.equals("n")) {
            System.out.println("---------------VISO_AUTORU_" + totalEntries + "-------------------\n");
        }
    }
}
