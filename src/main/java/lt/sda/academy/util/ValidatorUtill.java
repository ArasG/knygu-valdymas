package lt.sda.academy.util;

public class ValidatorUtill {

    public static boolean validateStringIsOnlyLetters(String string) {
        boolean isValid = true;
        char[] chars = string.toCharArray();
        for (char c : chars) {
            if (!Character.isLetter(c)) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }
}
