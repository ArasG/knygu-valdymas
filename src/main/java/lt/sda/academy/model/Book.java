package lt.sda.academy.model;


import lt.sda.academy.data.AuthorService;
import lt.sda.academy.util.HibernateUtil;
import org.hibernate.SessionFactory;

import javax.persistence.*;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String description;
    @ManyToOne// (fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Author author;

    public Book() {
    }

    public Book(String title, String description, int authorId) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        AuthorService authorService = new AuthorService(sessionFactory);
        Author requiredAuthor = authorService.getAuthorById(authorId);
        boolean checkIsAuthorNotExists = authorService.checkIfAuthorNotExistById(requiredAuthor);

        if (checkIsAuthorNotExists) {
            System.out.println(" \n---Autorius su tokiu ID numeriu neegzistuoja---\n");
            return;
        } else {
            this.author = requiredAuthor;
            this.title = title;
            this.description = description;
        }
    }

    public Book(String title, String description, Author author) {
        this.author = author;
        this.title = title;
        this.description = description;
    }


    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String toStringWithAuthorsName() {
        return "Id: " + id +
                "; Title: " + title +
                "; Author name: " + author.getFirstName() +
                "; Author surname: " + author.getLastName();
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public Author getAuthor() {
        return author;
    }
}
