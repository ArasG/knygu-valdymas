package lt.sda.academy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Review {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int bookId;
    private int score;
    String comment;


    public Review (){

    }

    public Review (int bookId, int score, String comment){
        this.bookId = bookId;
        this.score = score;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public int getBookId() {
        return bookId;
    }

    public int getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }


}
