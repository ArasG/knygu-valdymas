package lt.sda.academy.data;

import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;
import lt.sda.academy.model.Review;
import lt.sda.academy.presentation.BookChoice;
import lt.sda.academy.util.ScannerUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class BookService {

    private SessionFactory sessionFactory;
    AuthorService authorService;

    public BookService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        this.authorService = new AuthorService(sessionFactory);
    }


    public void saveBook() {
        String bookTitleFromUser = ScannerUtil.getBookTitleInputFromUser();
        String bookDescriptionFromUser = ScannerUtil.getBookDesciptionInputFromUser();
        int authorsIdFromUser = ScannerUtil.getAuthorIdInputFromUser();
        boolean checkIfAuthorNotExist = authorService.checkIfAuthorNotExistById(authorsIdFromUser);

        if (checkIfAuthorNotExist) {
            System.out.println(" \n---Autorius su tokiu ID numeriu neegzistuoja---\n");
            return;
        } else {
            Book bookToSave = new Book(bookTitleFromUser, bookDescriptionFromUser, authorsIdFromUser);
            Session session = sessionFactory.openSession();
            session.save(bookToSave);
            BookChoice.bookSavedSuccessfully(bookToSave);
        }
    }

    public void saveBook(Book book) {
        Session session = sessionFactory.openSession();
        session.save(book);
        session.close();
    }

    public void printAllBooks() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from Book");
        int numbering = 1;
        List<Book> books = query.list();
        for (Object book : books) {
            System.out.println(numbering + ". " + book.toString());
            numbering +=1;
        }
        System.out.println("VISO KNYGU: " + books.size() +"\n");
        transaction.commit();
    }

    public void updateBooksTitle(int booksId, String title) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("update Book b SET b.title =:booktitle WHERE b.id =: bookid");
        query.setParameter("booktitle", title);
        query.setParameter("bookid", booksId);
        query.executeUpdate();
        transaction.commit();
    }

    public void updateBooksDescription(int booksId, String description) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("update Book b SET b.description =:descr WHERE b.id =: bookid");
        query.setParameter("descr", description);
        query.setParameter("bookid", booksId);
        query.executeUpdate();
        transaction.commit();
    }

    public void updateBooksAuthor(int booksId, Author author) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("update Book b SET b.author =:newAuthor WHERE b.id =:booksid");
        query.setParameter("newAuthor", author);
        query.setParameter("booksid", booksId);
        query.executeUpdate();
        transaction.commit();
    }

    public Book getBookById(int id) {
        Session session = sessionFactory.openSession();
        Book book = session.get(Book.class, id);
        session.close();
        return book;
    }

    public Optional<Book> getBookByIdOptional(int id) {
        Session session = sessionFactory.openSession();
        return Optional.ofNullable(session.get(Book.class, id));
    }

    public void deleteBook(Book book) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(book);
        transaction.commit();
    }

    public List<Book> getBooksByAuthor(Author author) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from Book b WHERE author_id =:authorId");
        query.setParameter("authorId", author.getId());
        return query.list();
    }

    public Book getBookByTitleDescriptionAuthorId(String title, String description, int authorID) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Book b WHERE b.title =:title AND b.description =:description AND b.author.id =: authorId");
        query.setParameter("title", title);
        query.setParameter("description", description);
        query.setParameter("authorId", authorID);
        return (Book) query.uniqueResult();
    }

    public boolean checkIfBookNotExists(Book book, int authorID) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select count(*) from Book b WHERE b.title =:title AND b.author.id =:authorId");
        query.setParameter("title", book.getTitle());
        query.setParameter("authorId", authorID);
        long result = (Long) query.uniqueResult();
        //System.out.println("result:::" + result+ "author id::: " + authorID + "book titkle:::: " + book.getTitle());
        if (result == 1) {
            return false;
        } else {
            return true;
        }
    }

}
