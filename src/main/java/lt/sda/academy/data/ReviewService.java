package lt.sda.academy.data;

import lt.sda.academy.model.Book;
import lt.sda.academy.model.Review;
import lt.sda.academy.util.ScannerUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ReviewService {

    private SessionFactory sessionFactory;

    public ReviewService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveReview(int bookId, int score, String comment) {
        Review review = new Review(bookId, score, comment);
        Session session = sessionFactory.openSession();
        session.save(review);
    }

    public List<Review> getAllReviews() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Review");
        return query.list();
    }

    public void printAllReviewsWithBookTitle(Review review, Book book){
        System.out.println("ID:" + review.getId() +
                " // title:" + book.getTitle() +
                " // score: " + review.getScore() +
                " // comment:" + review.getComment() + "\n");
    }

    public String returnAllReviewsWithBookTitle(Review review, Book book){
        return "ID:" + review.getId() +
                " // title:" + book.getTitle() +
                " // score:" + review.getScore() +
                " // comment:" + review.getComment() + "\n";
    }

    public List<Review> getAllReviewsForOneBook(Book book) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Review r WHERE r.bookId =:idOfBook");
        query.setParameter("idOfBook", book.getId());
        return query.list();
    }

    public void deleteRevewByBookId(int idOfBook){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("delete from Review r WHERE bookId =:idOfBook");
        query.setParameter("idOfBook", idOfBook);
        transaction.commit();
    }
}
