package lt.sda.academy.data;

import lt.sda.academy.model.Author;
import lt.sda.academy.util.ScannerUtil;
import lt.sda.academy.util.ValidatorUtill;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class AuthorService {

    private SessionFactory sessionFactory;

    public AuthorService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public Author getAuthorById(int id) {
        Session session = sessionFactory.openSession();
        Author author = session.find(Author.class, id);
        session.close();
        return author;
    }

    public Optional<Author> getAuthorByIdOptional(int id) {
        Session session = sessionFactory.openSession();
        return Optional.ofNullable(session.find(Author.class, id));
    }


    public int getAuthorId(String firstName, String lastName) {
        Session session = sessionFactory.openSession();
        Query<Integer> query = session.createQuery("SELECT id from Author a where a.firstName = :name and a.lastName = :surname");
        query.setParameter("name", firstName);
        query.setParameter("surname", lastName);
        return query.uniqueResult();


    }

    public void saveAuthor(Author author) {
        Session session = sessionFactory.openSession();
        session.save(author);
        session.close();
    }


    public void saveAuthor() {
        String authorsFirstNameFromInput = ScannerUtil.getAuthorFirstNameInputFromUser();
        boolean inputIsValid = ValidatorUtill.validateStringIsOnlyLetters(authorsFirstNameFromInput);
        while (!inputIsValid) {
            System.out.println("Ivestas vardas turi buti sudarytas tik is raidziu. Bandykite dar karta. " +
                    "Nutraukti operacija: EXIT");
            authorsFirstNameFromInput = ScannerUtil.getStringInput();
            if (authorsFirstNameFromInput.toLowerCase().equals("exit")) {
                break;
            }
            inputIsValid = ValidatorUtill.validateStringIsOnlyLetters(authorsFirstNameFromInput);
        }

        if (!authorsFirstNameFromInput.equals("exit")) {
            String authorsLastNameFromInput = ScannerUtil.getAuthorLastNameInputFromUser();
            inputIsValid = ValidatorUtill.validateStringIsOnlyLetters(authorsLastNameFromInput);
            while (!inputIsValid) {
                System.out.println("Ivesta pavarde turi buti sudaryta tik is raidziu. Bandykite dar karta. " +
                        "Nutraukti operacija: EXIT");
                authorsLastNameFromInput = ScannerUtil.getStringInput();
                if (authorsLastNameFromInput.toLowerCase() == "exit") {
                    break;
                }
                inputIsValid = ValidatorUtill.validateStringIsOnlyLetters(authorsLastNameFromInput);
            }
            if (!authorsFirstNameFromInput.equals("exit") && !authorsLastNameFromInput.equals("exit")) {
                Author author = new Author(authorsFirstNameFromInput, authorsLastNameFromInput);
                Session session = sessionFactory.openSession();
                session.save(author);
                int authorId = getAuthorId(authorsFirstNameFromInput, authorsLastNameFromInput);
                System.out.println("\n-----Buvo sukurtas " + authorsFirstNameFromInput + " " + authorsLastNameFromInput +
                        ", kurio ID : " + authorId + "\n-----");
            }
        } else {
            return;
        }
    }

    public void printAllAuthors() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Author");
        List<Author> authors = query.list();
        System.out.println("\n--------------VISI_AUTORIAI--------------");
        for (Author author : authors) {
            System.out.println(author.getId() + ". " + author.getFirstName() + " " + author.getLastName());
        }
        System.out.println("-----------------------------------------\n");
    }

    public void printAuthorsPagination(int firstresult) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Author");
        List<Author> authors = query.setFirstResult(firstresult)
                .setMaxResults(10)
                .getResultList();
        if (firstresult == 0) {
            System.out.println("\n--------------RODOMA_PO_10_IRASU--------------");
        }
        for (Author author : authors) {
            System.out.println(author.getId() + ". " + author.getFirstName() + " " + author.getLastName());
        }
    }

    public long getTotalCountOfAuthors() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select count(id) from Author");
        return (Long) query.uniqueResult();
    }

    public Author getAuthorByNameAndSurname(String firstName, String lastName) {
        Session session = sessionFactory.openSession();
        Query<Author> query = session.createQuery("from Author a WHERE a.firstName =:name AND a.lastName =:surname");
        query.setParameter("name", firstName);
        query.setParameter("surname", lastName);
        return query.uniqueResult();

    }

    public void searchAuthorByNameOrSurname() {
        Session session = sessionFactory.openSession();
        System.out.println("Iveskit ieskomo autoriaus varda ar pavarde: ");
        String input = ScannerUtil.getStringInput();
        Query query = session.createQuery("from Author a WHERE a.firstName =:name OR a.lastName =:surname");
        query.setParameter("name", input);
        query.setParameter("surname", input);
        List<Author> authors = query.list();
        System.out.println("VISO SURASTA AUTORIU: " + authors.size());
        System.out.println("ID / Vardas / Pavarde");
        for (Author a : authors) {
            System.out.println(a.getId() + ". " + a.getFirstName() + " " + a.getLastName());
        }
    }

    public void deleteAuthor(Author author) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(author);
        transaction.commit();
    }

    public void updateAuthor(int id, String name, String surname) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("update Author a SET a.firstName =:name, a.lastName =:surname WHERE a.id =: authorid");
        query.setParameter("authorid", id);
        query.setParameter("name", name);
        query.setParameter("surname", surname);
        query.executeUpdate();
        transaction.commit();
    }

    // check author if exists
    public boolean checkIfAuthorNotExistById(Author author) {
        Session session = sessionFactory.openSession();
        Query<Author> query = session.createQuery("from Author a WHERE a.firstName =:firstName AND a.lastName =:lastName");
        query.setParameter("firstName", author.getFirstName());
        query.setParameter("lastName", author.getLastName());
        Author resultAuthor = query.uniqueResult();
        return resultAuthor == null;
    }

    public boolean checkIfAuthorNotExistById(int authorId) {
        Session session = sessionFactory.openSession();
        Author author = session.get(Author.class, authorId);
        return author == null;
    }
}





