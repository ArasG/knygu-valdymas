package lt.sda.academy;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.data.BookService;
import lt.sda.academy.data.ReviewService;
import lt.sda.academy.model.Author;
import lt.sda.academy.util.HibernateUtil;
import lt.sda.academy.util.ImportCsvUtil;
import lt.sda.academy.util.PaginationUtil;
import org.hibernate.SessionFactory;

public class TestClass {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        BookService bookService = new BookService(sessionFactory);
        AuthorService authorService = new AuthorService(sessionFactory);
        ReviewService reviewService = new ReviewService(sessionFactory);
        PaginationUtil paginationUtil = new PaginationUtil();
        ImportCsvUtil importCsvUtil = new ImportCsvUtil();


        //int intTest = authorService.getAuthorId("Jonas", "Jonaitis");
       // System.out.println(intTest);

    }
}
