package lt.sda.academy.presentation;

import lt.sda.academy.model.Book;
import lt.sda.academy.util.ScannerUtil;

public class BookChoice {
    public static int selectBookChoice() {
        System.out.println("1.Matyti visas knygas");
        System.out.println("2.Ieškoti knygų pagal pavadinimą arba autorių");
        System.out.println("3.Pridėti knygą");
        System.out.println("4.Atnaujinti knygą");
        System.out.println("5.Ištrinti knygą pagal id");
        System.out.println("6.Importuoti knygas su autoriais is failo");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static int updateBookChoice(){
        System.out.println("1. Atnaujinti knygos pavadinima");
        System.out.println("2. Atnaujinti knygos aprasyma");
        System.out.println("3. Atnaujinti knygos autoriu");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static void bookUpdatedSuccessfully(Book book){
        System.out.print("\n----Atnaujinimas sekmingas: ");
        System.out.println(book.toStringWithAuthorsName() + "\n");
    }

    public static void bookSavedSuccessfully(Book book){
        System.out.println("\n----Knyga issaugota: ");
        System.out.println(book.toStringWithAuthorsName()+ "\n");
    }




}
