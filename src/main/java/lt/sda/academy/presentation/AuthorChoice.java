package lt.sda.academy.presentation;

import lt.sda.academy.model.Author;
import lt.sda.academy.util.ScannerUtil;

import java.sql.SQLOutput;

public class AuthorChoice {
    public static int selectAuthorsChoices() {
        System.out.println("1. Matyti visus autorius");
        System.out.println("2. Ieškoti autorių pagal vardą arba pavardę ");
        System.out.println("3. Pridėti autorių");
        System.out.println("4. Atnaujinti autorių ");
        System.out.println("5. Ištrinti Autorių pagal id");
        System.out.println("6. Istrinti autoriu ir jo knygas");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static int enterAuthorToDeleteId() {
        System.out.println("----Iverskite norimo istrinti autoriaud id: ");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static void authorDeletedSuccessfully(Author author) {
        System.out.println("\n----Autorius: " + author.toIdStringNameSurname() + " istrintas----");
    }

    public static String enterFirstNameOfAuthorToUpdate() {
        System.out.println("----Irasykite autoriaus nauja varda: ");
        return ScannerUtil.SCANNER.next();
    }

    public static String enterLastNameOfAuthorToUpdate() {
        System.out.println("----Irasykite autoriaus nauja pavarde: ");
        return ScannerUtil.SCANNER.next();
    }

    public static int enterAuthorToUpdateId() {
        System.out.println("----Iverskite norimo koreguoti autoriaud id: ");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static void authorUpdatedSuccessfully(Author author, String name, String surname) {
        System.out.println("\n----Autorius: " + author.toIdStringNameSurname() + " atnaujintas " +
                " i: " + name + " " + surname + " ----\n");
    }
}
